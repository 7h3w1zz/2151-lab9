package cpsc2150.listDec;

import java.util.List;
import java.util.Random;

/**
 * @pre swaps >= 0
 * @post Two elements' positions have been swapped swaps number of times AND
 *       self contains the same items as #self
 */
public interface IShuffleList<T> extends List<T> {

    /**
     * @pre swaps >= 0
     * @post Two elements' positions have been swapped swaps number of times AND
     *       self contains the same items as #self
     * @param swaps The number of times to swap elements in the list
     */
    default void shuffle(int swaps) {
        Random rand = new Random();
        for (int i = 0; i < swaps; i++) {
            swap(rand.nextInt(size()), rand.nextInt(size()));
        }
    }

    /**
     * @pre 0 <= i < size() AND
     *      0 <= j < size()
     * @post self = #self except the elements at indexes i and j have been swapped
     * @param i The first element to swap
     * @param j The second element to swap
     */
    default void swap(int i, int j) {
        T tmp = get(i);
        set(i, get(j));
        set(j, tmp);
    }
}
